


// [Pages]
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';


// import {Fragment} from 'react';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import { useState } from 'react';
import { UserProvider } from './UserContext';



function App() {



  // State hook for the user that will be globally accessible using the useContext
  // This will also be used to store the user information and be used for validating if a user is logged in on the app or not

    const [user, setUser] = useState(localStorage.getItem("email"));
    
    // Function for clearing local storage on logout
    const unSetUser =()=>{
      localStorage.removeItem("email");
      setUser(null);
    }
 
    // <UserProvider value = {{user,setUser, unSetUser}}> Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop

    // All information provided to the Provider component can be accessed later on from the context object as properties

  return (
    <UserProvider value = {{user,setUser, unSetUser}}>
    <Router>
      <AppNavbar/>
      <Routes>
     
        <Route  path="/" element ={<Home/>}/>
        <Route  path="/courses" element ={<Courses/>}/>
        <Route  path="/register" element ={<Register/>}/>
        <Route  path="/login" element ={ <Login />}/>
        <Route  path="/logout" element ={ <Logout/>}/>
        <Route  path="*" element ={ <Error/>}/>
      </Routes>
    </Router>
      
    </UserProvider>
   
  );
}

export default App;
