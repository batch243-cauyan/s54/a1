import {Fragment} from 'react'
import { Row,Col, Container} from 'react-bootstrap';
import CourseCard from '../components/CourseCard';
import courseData from '../data/courses.js';

export default function Courses(){
	
	// Syntax:
	// localStoraget.getItem("propertyName")
	// localStorage.getItem('email);


	// const local = localStorage.getItem('email');
	// console.log(local);

	const courses = courseData.map(course => {
		return(
				<CourseCard key = {course.id} prop ={course}/>
			)
	})

	
	return(

		<Row>
			<Col>
				{courses}
			</Col>
		</Row>

		)
}