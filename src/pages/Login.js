import { useState, useEffect,useContext } from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    // Allows us to consume the User context object and its properties to use for user validation
    const {user, setUser} = useContext(UserContext);

    // const [user,setUser] = useState(null);
    

    useEffect(()=>{
        if (email && password){
            setIsActive(true);
        }else{
            setIsActive(false)
    }
    },[email,password])


    function loginUser(event){
        event.preventDefault();
        alert("Logged In Successfully.")


        // Storing information in the local storage will make the data persistent even as the page is refreshed unlike with the use of states where information is reset when page is reloaded.
        localStorage.setItem('email',email);

        // Set the global user state to have properties obtained from the local storage.
        // though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing.

        setUser(localStorage.getItem("email"));
        setEmail('');
        setPassword('');
    }

    // function authenticate(event){
    //     event.preventDefault();
    //     alert("Logged In Successfully.")
    //     localStorage.setItem('email',email);

    //     localStorage.setItem('token',token);

    // }



    return (
        
            (user) ? 
            <Navigate to ="/"/>
            :
                <Container>
                        <Row>
                            <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                                <Form className='p-3' onSubmit={loginUser}>
                                    <h1>Login</h1>
                                    <Form.Group className="mb-3" controlId="email">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control type="email" placeholder="Enter email" required value={email} onChange={event =>{
                                            setEmail(event.target.value)
                                        }}/>
                                        
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="password1">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" required value={password} onChange={event =>{
                                            setPassword(event.target.value)
                                        }}/>

                                    </Form.Group>

                                    <Button className ="btn-lg"variant="success" type="submit" disabled={!isActive} >
                                        Login
                                    </Button>
                                </Form>
                        </Col>
                    </Row>
                </Container>
        
     );
}
 
export default Login;