import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect } from "react";

const Logout = () => {

    const {unSetUser} = useContext(UserContext);

    
    
    // useEffect(()=>{
    //     unSetUser();
    // })
    unSetUser();
    return ( 
        <Navigate to ="/login"/>
     );
}
 
export default Logout;